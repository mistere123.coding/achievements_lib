local S = minetest.get_translator("achievements_lib")

----------------------------------------------
-----------      Init tables     -------------
----------------------------------------------

achievements = {}                 -- KEY: ach_name; VALUE: {properties}
mods = {}                         -- KEY: mod_name; VALUE: name, icon

p_achievements = {}               -- KEY: p_name; VALUE: {ach_name1 = true, ach_name2 = true}
p_ach_log = {}                    -- KEY: p_name; VALUE: {1 = ach_name1, 2 = ach_name2} -- to track the order of achievements unlocked
player_ach_backlog = {}           -- KEY: p_name; VALUE: {1 = ach_name1, 2 = ach_name2} -- to temporarily track the order of achievements unlocked whilst offline

local storage = minetest.get_mod_storage("achievements_lib")

----------------------------------------------
-----------Predeclare local funcs-------------
----------------------------------------------

-- storage
local function store_player_achievements(p_name) end
local function recall_player_achievements(p_name) end

-- backlog to show hud popups when player logs on if they have not seen the
-- popups yet (awarded while offline; maybe team challenges were met, or they
-- won a tournament or something)
local function store_achievement_backlog(p_name) end
local function recall_achievement_backlog(p_name) end

local function try_to_load_player(p_name) end

----------------------------------------------
----------------    API     ------------------
----------------------------------------------

function achvmt_lib.register_mod(mod, def)
  assert(not mods[mod], "Non puoi registrare il minigioco due volte!")
  mods[mod] = {}

  local mod_ref = mods[mod]

  mod_ref.name = def.name
  mod_ref.icon = def.icon
  --mod_ref.ach_amount = 0
end



function achvmt_lib.register_achievement(name, def)
  local splitter_pos = string.find(name, ":")
  assert(splitter_pos, "[Achievements_lib] Il prestigio " .. name .. " non segue la sintassi nomemod:nomeprestigio!")

  local mod = name:sub(1, splitter_pos -1)
  assert(mods[mod], "[Achievements_lib] Non esiste una mod " .. mod .. " registrata per supportare i prestigi!")
  assert(def.title, "[Achievements_lib] Il titolo è obbligatorio!")

  local ach = {}

  -- for easy cross-reference
  ach.mod = mod
  ach.name = name
  -- all the rest
  ach.title = def.title
  ach.description = def.description or ""
  ach.image = def.image or "achievements_lib_item.png"
  ach.tier = def.tier or "Bronze"
  ach.hidden = def.hidden or def.secret or false
  ach.on_award = def.on_award or function(p_name) return end
  ach.on_unaward = def.on_unaward or function(p_name) return end

  --mods[mod].ach_amount = mods[mod].ach_amount +1

  achievements[name] = ach
end



function achvmt_lib.award(ach_name, p_name)
  -- can't award to a player that does not exist
  local player_exists, is_offline = try_to_load_player(p_name)
  if not player_exists then return end

  -- can't award an achievement that does not exist
  if not achvmt_lib.exists(ach_name) then return end

  -- can't award an achievement the player already has.
  if achvmt_lib.has_achievement(p_name, ach_name) then return end

  -- give achievement and save it
  p_achievements[p_name][ach_name] = true
  table.insert(p_ach_log[p_name], ach_name)

  local ach_def = achievements[ach_name]

  -- store popup backlog if player is offline, or show popup now if they are
  -- online
  if is_offline then
    table.insert(player_ach_backlog[p_name], ach_name)
    store_achievement_backlog(p_name)
  else
    achvmt_lib.hud_show(p_name, ach_def)
  end

  ach_def.on_award(p_name)

  store_player_achievements(p_name)
  return true
end



function achvmt_lib.unaward(ach_name, p_name)
  if not try_to_load_player(p_name) then return end

  if achvmt_lib.has_achievement(p_name, ach_name) then
    if achievements[ach_name] then
      achievements[ach_name].on_unaward(p_name)
    end

    p_achievements[p_name][ach_name] = nil
    for i, ach_name_log in ipairs(p_ach_log) do
      if ach_name_log == ach_name then
        table.remove(p_ach_log[p_name], i)
        break
      end
    end

    store_player_achievements(p_name)
    return true
  end
end



function achvmt_lib.unaward_all(p_name)
  if not try_to_load_player(p_name) then return end

  for ach_name, _ in pairs(p_achievements[p_name]) do
    if achievements[ach_name] then
      achievements[ach_name].on_unaward(p_name)
    end
  end

  p_achievements[p_name] = {}
  store_player_achievements(p_name)

  return true
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function achvmt_lib.has_achievement(p_name, ach_name)
  if not try_to_load_player(p_name) then return end

  return p_achievements[p_name][ach_name]
end



function achvmt_lib.exists(ach_name)
  return achievements[ach_name] ~= nil
end



-- INTERNAL USE ONLY
function achvmt_lib.load_achievements(p_name, is_first_access)
  if is_first_access then
    p_achievements[p_name] = {}
    p_ach_log[p_name] = {}
    player_ach_backlog[p_name] = {}
    store_player_achievements(p_name)

  else
    recall_player_achievements(p_name)
    recall_achievement_backlog(p_name) -- TODO
    --local backlog = achvmt_lib.player_ach_backlog[p_name]
    -- TODO: show backlog in for loop.
    -- delete backlog, save.
    player_ach_backlog[p_name] = {}
  end

  store_achievement_backlog(p_name)
end





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function achvmt_lib.get_mods()
  local ach_mods = {}
  for mod, _ in pairs(mods) do
    table.insert(ach_mods, mod)
  end
  return ach_mods
end



function achvmt_lib.get_mod_info(mod_name)
  return mods[mod_name]
end



function achvmt_lib.get_achievements(as_def, filter)
  if not filter and as_def then return achievements end

  local achs = {}
  filter = filter or {}

  for name, ach_def in pairs(achievements) do
    local include = true

    if filter.mod and ach_def.mod ~= filter.mod then
      include = false
    end
    if filter.tier and ach_def.tier ~= filter.tier then
      include = false
    end

    if include then
      if as_def then
        achs[name] = ach_def
      else
        table.insert(achs, name)
      end
    end
  end

  return achs
end



function achvmt_lib.get_player_achievements(p_name, as_def, filter)
  local player_exists = try_to_load_player(p_name)

  if not player_exists then return end
  if not filter and not as_def then return p_achievements[p_name] end

  local ret = {}

  for ach_name, _ in pairs(p_achievements[p_name]) do
    local ach_def = achievements[ach_name]

    if ach_def then
      local include = true

      if filter.mod and ach_def.mod ~= filter.mod then
        include = false
      end

      if filter.tier and ach_def.tier ~= filter.tier then
        include = false
      end

      if include then
        if as_def then
          ret[ach_name] = ach_def
        else
          table.insert(ret, ach_name)
        end
      end
    end
  end

  return ret
end



function achvmt_lib.get_latest_unlocked(p_name, amount)
  local player_exists = try_to_load_player(p_name)

  if not player_exists then return end

  local latest = {}

  for i = 1, math.min(amount, #p_ach_log[p_name]) do
    table.insert(latest, p_ach_log[p_name][i])
  end

  return latest
end





----------------------------------------------
----------- Local funcs defined --------------
----------------------------------------------

function store_player_achievements(p_name)
  local ach = minetest.serialize(p_achievements[p_name])
  local log = minetest.serialize(p_ach_log[p_name])

  storage:set_string("achvmts_" .. p_name, ach)
  storage:set_string("log_" .. p_name, log)
end



function recall_player_achievements(p_name)
  local ach = storage:get_string("achvmts_" .. p_name)
  local log = storage:get_string("log_" .. p_name, log)

  p_achievements[p_name] = minetest.deserialize(ach) or {}
  p_ach_log[p_name] = minetest.deserialize(log) or {}
end



function store_achievement_backlog(p_name)
  local ser = minetest.serialize(player_ach_backlog[p_name])
  storage:set_string("backlog_" .. p_name, ser)
end



function recall_achievement_backlog(p_name)
  local ser = storage:get_string("backlog_" .. p_name)
  player_ach_backlog[p_name] = minetest.deserialize(ser) or {}
end



function try_to_load_player(p_name)
  local player_exists = true
  local is_offline = false

  if not p_achievements[p_name] then
    recall_player_achievements(p_name)
    is_offline = true
  end

  if not p_achievements[p_name] then
    player_exists = false
    is_offline = false
  end

  return player_exists, is_offline
end
