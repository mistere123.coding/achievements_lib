
Achievements_lib is a library that handles achievements. It provides a system
for awarding achievements and viewing them, as well as a HUD notification on
award.

## Registering an achievement

`achvmt_lib.register_mod(mod_name, def)`: registers a mod that will contain achievements.
`def` is a table containing the following fields:
* `name`: (string) the name of the mod. Contrary to `mod_name`, this is not the technical name and it can be translated
* `icon`: (string) the icon representing the mod

`achvmt_lib.register_achievement(technical_name, achievement_def)`: registers an achievement. `technical_name` is a string that must follow the syntax `mod_name:whatever` (name must be unique). `achievement_def` is a table with the following fields:
* `title`: (string) the title of the achievement
* `description`: (string) the description of the achievement
* `image`: (string) the image of the achievement. Should be 26x26
* `tier`: (string) the tier of the string. Can be either `"Bronze"`, `"Silver"` or `"Gold"`
* `hidden`: (boolean) whether to hide the description until the unlocked. `false` by default
* `secret`: (boolean) whether to completely hide the achievement (the title will be "Secret Achievement" and there will be a placeholder image until unlocked). `false` by default
* `on_award`: (function(p_name)): function run when awarded
* `on_unaward`: (function(p_name)): function run when unawarded

Example:

```lua
  {
    title = "Ultimate Bounciness",
    description = "Bounce 50 times",
    image = "mymod_ultimate_bounciness.png",
    tier = "Gold"
  }
  ```

  After registration, when an achievement definition is returned, it will also
  contain the indexes `mod` and `name` for easy reference.

## Awarding and removing achievements

* `achvmt_lib.award(ach_name, p_name)`: awards an achievement to `p_name`. If the player is not online, then they will receive a hud popup when they next log on. Returns `true` on success, `false` on failure.
* `achvmt_lib.unaward(ach_name, p_name)`: removes an achievement from a player. Returns `true` on success
* `achvmt_lib.unaward_all(p_name)`: same as `unaward` but for every achievement


## Getters

Some getters take an optional `filter` parameter. `filter` is a table to filter the output and it's structured as such:
```lua
  {
    mod = "My mod", -- modname
    tier = "Bronze", -- tier name
  }
  ```

* `achvmt_lib.get_achievements(<as_def>, <filter>)`: returns a table of achievements. If `as_def` is false, it'll have achievement names as value (`{"ach_name1", "ach_name2"}`). Otherwise, it'll return the whole definition with names as key (`{ach_name1 = def1, ach_name2 = def2}`)
* `achvmt_lib.get_player_achievements(p_name, <as_def>, <filter>)`: like `get_achievements(..)` but only for the ones unlocked by `p_name`
* `achvmt_lib.get_latest_unlocked(p_name, amount)`: returns a table with the latest `amount` achievements unlocked by `p_name`, starting from the newest to the oldest
* `achvmt_lib.get_mods()`: returns a table with registered mod names as a value
* `achvmt_lib.get_mod_info(mod)`: returns a table containing the information provided when the mod was registered. `nil` if it doesn't exist


## Utils

* `achvmt_lib.has_achievement(p_name, ach_name)`: returns whether the player has the specified achievement
* `achvmt_lib.exists(ach_name)`: returns whether `ach_name` exists


## GUI

* `achvmt_lib.hud_show(p_name, achievement_def)`: shows a HUD popup to `p_name` when they are awarded an achievement. Override this function to change the default behaviour
* `achvmt_lib.gui_show(p_name, to_p_name)` shows `to_p_name` a menu with a scrollable list of `p_name` achievements. If `to_p_name` is not specified, it'll default to `p_name`. Override this function to implement custom behavior.


## About the Authors:
Zughy and Friends is an alliance of free software enthusiasts who make minetest
content. Our minigame server is Arcade Emulation Server, and most of the content
on it is by us. Donate to support our work, especially if this library was
useful to you: https://liberapay.com/Zughy/
